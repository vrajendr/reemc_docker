# reemc_docker

Dockerfile to build a workspace for prototyping with the REEM-C in simulation.

To use:

1. `make` (to build the Docker container -> takes ~20 mins)
2. `./start.sh` to start the container