source /opt/ros/kinetic/setup.bash
source /reemc_public_ws/devel/setup.bash
source /home/$USER/catkin_ws/devel/setup.bash
alias killros='killall -9 gazebo & killall -9 gzserver  & killall -9 gzclient & killall -9 roslaunch & killall -9 roscore & killall -9 rosmaster & killall -9 rviz & killall -9 robot_state_publisher & killall -9 python & pkill -f "/home/${USER}/catkin_ws/devel/lib" & killall -9 "/home/${USER}/catkin_ws/devel/lib/*/*" & pkill -f /opt/ros/kinetic & killall -9 python2' 

#export PYTHONPATH=/workspace/install/lib/python2.7/dist-packages:$PYTHONPATH

#export GAZEBO_MODEL_PATH=/home/$USER/catkin_ws/src/hamp_masters_thesis_tests/models:$GAZEBO_MODEL_PATH
