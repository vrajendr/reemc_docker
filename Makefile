SHELL := /bin/bash

base:
	docker build --network=host -t uw_reemc_base -f DockerfileBase .

add-talos:
	docker build -t uw_reemc_talos -f DockerfileAddTalos --build-arg BASE_IMAGE=uw_reemc_base .
